module.exports = {
    "env": {
        "browser": true,
        "commonjs": true,
        "es6": true,
				"node": true,
    },
    "extends": [
			"eslint:recommended",
			"plugin:jest/all"
		],
    "globals": {
        "Atomics": "readonly",
        "SharedArrayBuffer": "readonly"
    },
    "parserOptions": {
        "ecmaVersion": 2018
    },
		"plugins": [
			"jest"
		],
    "rules": {
			"jest/prefer-expect-assertions": "off",
    }
};
