const {
	parseInputFileIntoGraph,
	priv: { getErrorMessageForInvalidLine, parseLine }
} = require('./parseInputFileIntoGraph.js');


describe('parseInputFileIntoGraph', function() {
	const path = 'path.txt';
	const libA = 'libA';
	const libB = 'libB';
	const libC = 'libC';
	const libD = 'libD';
	const expectedDepGraph = new Map([
		[ libB, new Set([ libC, libD ]) ],
		[ libA, new Set([ libB, libC ]) ],
		[ libD, new Set() ],
	]);


	it('should parse input into a corresponding graph', async function() {
		const lines = [
			`${libB} depends on   ${libC}   ${libD} `,
			` 	${libA} depends on ${libB}  ${libC}`,
			` 	${libD} depends on   `,
		];

		const depGraph = await parseInputFileIntoGraph(path, {
			fs: { createReadStream: () => {} },
			readline: { createInterface: () => lines },
		});

		expect(depGraph).toStrictEqual(expectedDepGraph);
	});


	it('should ignore blank lines', async function() {
		const lines = [
			`${libB} depends on   ${libC}   ${libD} `,
			` 	${libA} depends on ${libB}  ${libC}`,
			'',
			` 	${libD} depends on   `,
			'',
		];

		const depGraph = await parseInputFileIntoGraph(path, {
			fs: { createReadStream: () => {} },
			readline: { createInterface: () => lines },
		});

		expect(depGraph).toStrictEqual(expectedDepGraph);
	});


	it('should use passed fs module', function() {
		const createReadStreamMock = jest.fn();

		parseInputFileIntoGraph(path, {
			fs: { createReadStream: createReadStreamMock },
			readline: { createInterface: () => [] },
		});

		expect(createReadStreamMock).toHaveBeenCalledTimes(1);
		expect(createReadStreamMock).toHaveBeenCalledWith(path);
	});


	it('should use passed readline module', function() {
		const createInterfaceMock = jest.fn(() => []);
		const stream = 'fake';

		parseInputFileIntoGraph(path, {
			fs: { createReadStream: () => stream },
			readline: { createInterface: createInterfaceMock }
		});

		expect(createInterfaceMock).toHaveBeenCalledTimes(1);
		expect(createInterfaceMock).toHaveBeenCalledWith({
			input: stream,
			crlfDelay: Infinity,
		});
	});
});


describe('parseLine', function() {
	const expectedLibName = 'justLib';


	it('should parse a line as expetced', function() {
		const depA = 'depA';
		const depB = 'depB';
		const depC = 'depC';
		const line =
			`  	${expectedLibName} depends   on ${depA}  ${depC}		${depB} `;

		const [ libName, deps ] = parseLine(line);

		expect(libName).toStrictEqual(expectedLibName);
		expect(deps).toStrictEqual([ depA, depC, depB ]);
	});


	it('should parse line w/o deps', function() {
		const line =
			`  	${expectedLibName} depends   on	  `;

		const [ libName, deps ] = parseLine(line);

		expect(libName).toStrictEqual(expectedLibName);
		expect(deps).toHaveLength(0);
	});


	it('should throw on invalid line', function() {
		const line = 'abolish suffering';

		expect(() => parseLine(line)).toThrow(
			new Error(getErrorMessageForInvalidLine(line)));
	});
});
