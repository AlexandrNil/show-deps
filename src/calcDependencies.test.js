const { calcDependencies, priv: { traverseDeps } } =
	require('./calcDependencies.js');
const { priv: { DEP_NAME_RE, LIB_DEPS_SEPARATOR_EXCLUSION_RE } } =
	require('./parseInputFileIntoGraph.js');


describe('calcDependencies', function() {
	it('should preserve the order of top libs', function() {
		const orderedLibs = [ 'lib42', 'algo', 'N' ];
		const depGraph = new Map(orderedLibs.map(
			(libName) => [ libName, new Set() ]));

		const depLines = calcDependencies(depGraph);

		const everyInOrder = depLines.every(
			(depLine, i) => depLine.startsWith(`${orderedLibs[i]} `));
		expect(everyInOrder).toStrictEqual(true);
	});


	it('should handle libs w/o deps', function() {
		const libName = [ 'lib42' ];
		const depGraph = new Map([ [ libName, new Set() ] ]);

		const depLine = calcDependencies(depGraph)[0];

		expect(depLine).toStrictEqual(`${libName} has no dependencies`);
	});


	it('should not repeate deps', function() {
		const libName = 'libX';
		const anotherLibName = 'G';
		const commonDep = 'openGX';
		const depGraph = new Map([
			[ libName, new Set([ anotherLibName, commonDep, 'Y' ]) ],
			[ anotherLibName, new Set([ commonDep, 'F' ]) ],
		]);

		const depLines = calcDependencies(depGraph);

		expect(depLines).toHaveLength(depGraph.size);
		expect(depLines.every(hasNoDuplicateDeps))
			.toStrictEqual(true);
	});


	function hasNoDuplicateDeps(depLine) {
		const [ , , depsStr ] = depLine.match(LIB_DEPS_SEPARATOR_EXCLUSION_RE);
		const deps = depsStr.match(DEP_NAME_RE);
		return deps.length === new Set(deps).size;
	}
});


describe('traverseDeps', function() {
	it('should return all deps for a given lib', function() {
		const givenLib = 'lib42';
		const dependeeLib = 'WPLib';
		const anotherDependeeLib = 'JamLib';
		const depGraph = new Map([
			[ givenLib, new Set([ dependeeLib, anotherDependeeLib, 'Y' ]) ],
			[ dependeeLib, new Set([ anotherDependeeLib, 'X' ]) ],
			[ anotherDependeeLib, new Set([ 'Z' ]) ],
		]);
		const expectedDeps = new Set(
			[ dependeeLib, anotherDependeeLib, 'Y', 'X', 'Z' ]);

		const deps = traverseDeps(givenLib, depGraph);

		expect(deps).toStrictEqual(expectedDeps);
	});


	it('should return an empty Set on no deps', function() {
		const depGraph = new Map();

		const deps = traverseDeps('libX', depGraph);

		expect(deps.size).toStrictEqual(0);
	});


	// FIXME: cover more cases
	describe('with cycles', function() {
		const libA = 'libA';


		it('should return self-dependency', function() {
			const depGraph = new Map([
				[ libA, new Set([ libA ]) ]
			]);

			const deps = traverseDeps(libA, depGraph);

			expect(deps).toStrictEqual(new Set([ libA ]));
		});


		it.skip('should warn', function() {
			const libB = 'libB';
			const libC = 'libC';
			const depGraph = new Map([
				[ libA, new Set([ libB, libC ]) ],
				[ libB, new Set([ libA ]) ],
				[ libC, new Set([ libB ]) ],
			]);
			const warnMock = jest.fn();
			console.warn = warnMock;

			traverseDeps(libC, depGraph);

			expect(warnMock).toHaveBeenCalledTimes(1);

			warnMock.mockClear();
		});
	});
});
