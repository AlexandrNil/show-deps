const DEP_SEPARATOR = ' ';


function calcDependencies(depGraph) {
	return Array.from(depGraph.entries())
		.reduce((outputAcc, [ libName, immediateDeps]) => {
		// TODO: refactor w/ optional chaining when it comes to Node
		outputAcc.push(immediateDeps && immediateDeps.size ?
			`${libName} depends on ${Array.from(traverseDeps(libName, depGraph)).join(DEP_SEPARATOR)}`
			:	`${libName} has no dependencies`);
		return outputAcc;
	}, []);
}


function traverseDeps(libName, depGraph) {
	const deps = new Set();
	const visited = { [libName]: true };
	const dfsStack = [ depGraph.get(libName) ];

	do {
		const immediateDeps = dfsStack.pop();

		if (immediateDeps === undefined) {
			continue;
		}

		immediateDeps.forEach((dep) => {
			// FIXME: it warns for non-cycles, too. Unskip the corresponding test.
			// if (visited[dep]) {
			// 	console.warn(
			// 		`Warning: found a dependency cycle on "${dep}" while tracing dependencies for "${libName}"`);
			// }
			deps.add(dep);
			if (depGraph.has(dep) && !visited[dep]) {
				dfsStack.push(
					depGraph.get(dep));
				visited[dep] = true;
			}
		});
	} while (dfsStack.length);

	return deps;
}


exports.calcDependencies = calcDependencies;

exports.priv = {
	traverseDeps,
};
