const DEP_NAME_RE = /\w+/g;
const LIB_DEPS_SEPARATOR_EXCLUSION_RE =
	/(\w+)\s*depends\s+on\s*(\w+.*)*/i;


async function parseInputFileIntoGraph(path, { fs, readline }) {
	const asyncLines = readline.createInterface({
    input: fs.createReadStream(path),
    crlfDelay: Infinity
  });
	// We're using Map to preserve the insertion order.
	const depGraph = new Map();

	for await (const line of asyncLines) {
		if (line.trim().length) {
			const [ libName, immediateDeps ] = parseLine(line);
			depGraph.set(libName, new Set(immediateDeps));
		}
  }

	return depGraph;
}

function parseLine(line) {
	const lineMatch = line.match(LIB_DEPS_SEPARATOR_EXCLUSION_RE);
	if (lineMatch) {
		// ignoring the first element, which is the entire matched string
		const [ , libNameRaw, depsStr = '' ] =
			line.match(LIB_DEPS_SEPARATOR_EXCLUSION_RE);
		return [ libNameRaw.trim(), depsStr.match(DEP_NAME_RE) || [] ];
	}

	throw new Error(getErrorMessageForInvalidLine(line));
}

function getErrorMessageForInvalidLine(line) {
	return `Unexpected format of the following line: "${line}"`;
}


exports.parseInputFileIntoGraph = parseInputFileIntoGraph;

exports.priv = {
	DEP_NAME_RE,
	LIB_DEPS_SEPARATOR_EXCLUSION_RE,
	getErrorMessageForInvalidLine,
	parseLine,
};
