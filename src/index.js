#!/usr/bin/env node
const fs = require('fs');
const readline = require('readline');
const { calcDependencies } = require('./calcDependencies.js');
const { parseInputFileIntoGraph } =
	require('./parseInputFileIntoGraph.js');

main();

// FIXME: write functional tests
// FIXME: warn on repeated (top) libraries
async function main() {
	if (process.argv[2] === undefined) {
		// TODO: implement --help option
		console.error('Error: no input file name was given.');
		return;
	}

	try {
		calcDependencies(
			await parseInputFileIntoGraph(process.argv[2], { fs, readline })
		).forEach((outputLine) => { console.log(outputLine); });
	} catch ({ message }) {
		console.error(`Error: ${message}`);
	}
}
