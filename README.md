A CLI tool for one interview assignment.

## Setup

Clone the repo & `cd` into the cloned folder. Then:

`npm install`

## Running the tool

`node src/index.js <full path to a file w/ input like the files in the "data/input/" folder>`

OR

FIXME: write instructions about how to install `show-deps` as a CLI tool.

## Run test

`npm run test`

or with the code coverage:

`npm run test:coverage`

## Run JS code linting

`npm run lint`

## Contributing

Search for `FIXME` and `TODO` in the code and see whether you can help. PRs are welcomed :)
